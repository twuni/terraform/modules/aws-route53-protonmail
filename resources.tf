resource "aws_route53_record" "dkim1" {
  zone_id = data.aws_route53_zone.domain.zone_id
  name    = "protonmail._domainkey"
  type    = "CNAME"
  ttl     = "5"
  records = ["protonmail.domainkey.${var.dkim_id}.domains.proton.ch."]
}

resource "aws_route53_record" "dkim2" {
  zone_id = data.aws_route53_zone.domain.zone_id
  name    = "protonmail2._domainkey"
  type    = "CNAME"
  ttl     = "5"
  records = ["protonmail2.domainkey.${var.dkim_id}.domains.proton.ch."]
}

resource "aws_route53_record" "dkim3" {
  zone_id = data.aws_route53_zone.domain.zone_id
  name    = "protonmail3._domainkey"
  type    = "CNAME"
  ttl     = "5"
  records = ["protonmail3.domainkey.${var.dkim_id}.domains.proton.ch."]
}

resource "aws_route53_record" "mx" {
  zone_id = data.aws_route53_zone.domain.zone_id
  name    = data.aws_route53_zone.domain.name
  type    = "MX"
  ttl     = "5"
  records = [
    "10 mail.protonmail.ch.",
    "20 mailsec.protonmail.ch."
  ]
}
